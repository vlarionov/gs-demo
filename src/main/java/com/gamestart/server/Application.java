package com.gamestart.server;

import com.gamestart.server.model.bots.Bot;
import com.gamestart.server.model.user.Authority;
import com.gamestart.server.repository.AuthorityRepository;
import com.gamestart.server.repository.BotRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Application {
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner setup(
            AuthorityRepository authorityRepository,
            BotRepository botRepository
    ) {
        return args -> {

            if (botRepository.findAll().size() > 0) {
                return;
            }
            botRepository.save(new Bot(
                    null,
                    "76561198274515364",
                    "batya_char",
                    "steamPass123456",
                    "0Eo3KbITI3\\/bMyvjIoR4hahdF28=",
                    "vRM4tC13Vcl1MYOXYSXDl6pcXXY=",
                    "https://steamcommunity.com/tradeoffer/new/?partner=314249636&token=HYnTsAvb"
            ));
            /*
            botRepository.save(new Bot(
                    null,
                    "rito4korvet21",
                    "d2ovadima1q1q",
                    "KSTjNOkTd\\/RIMryKFRXaq6P52N4=",
                    "Q7PZaC8pQMTiuvEhWws9KkWEdq8=",
                    "https://steamcommunity.com/tradeoffer/new/?partner=1&token=q"
            ));
            */

            authorityRepository.save(new Authority(
                    null,
                    "PLAYER"
            ));
        };
    }

}
