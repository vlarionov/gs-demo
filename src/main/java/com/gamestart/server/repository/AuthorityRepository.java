package com.gamestart.server.repository;

import com.gamestart.server.model.user.Authority;

import java.util.Optional;

public interface AuthorityRepository extends EntityRepository<Authority> {
    Optional<Authority> findOneByRole(String role);
}
