package com.gamestart.server.repository;

import com.gamestart.server.model.SteamGame;
import com.gamestart.server.model.item.ItemMarketInfo;

public interface ItemMarketInfoRepository extends EntityRepository<ItemMarketInfo> {
    ItemMarketInfo findOneBySteamGameAndMarketHashName(
            SteamGame steamGame,
            String marketHashName
    );
}
