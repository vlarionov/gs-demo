package com.gamestart.server.repository;

import com.gamestart.server.model.item.TradeOperation;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface TradeOperationRepository extends EntityRepository<TradeOperation> {
    List<TradeOperation> findAllByStatus(TradeOperation.Status status);

    default List<TradeOperation> findAllCreated() {
        return findAllByStatus(TradeOperation.Status.CREATED);
    }

    Page<TradeOperation> findAllByUser_IdAndTypeOrderByDateDesc(
            Long userId,
            TradeOperation.Type type,
            Pageable pageable
    );

    TradeOperation findOneByOfferId(String offerId);
}
