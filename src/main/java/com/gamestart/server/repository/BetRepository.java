package com.gamestart.server.repository;

import com.gamestart.server.model.game.Bet;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

//@RepositoryRestResource
public interface BetRepository extends EntityRepository<Bet> {
    Page<Bet> findAllByUser_IdOrderByDateDesc(Long userId, Pageable pageable);

    List<Bet> findAllByUser_Id(Long userId);
}
