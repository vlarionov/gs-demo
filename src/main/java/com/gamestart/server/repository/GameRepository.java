package com.gamestart.server.repository;


import com.gamestart.server.model.game.Game;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

//@RepositoryRestResource
public interface GameRepository extends EntityRepository<Game> {
    Game findOneByIdAndFinishDateIsNotNull(Long id);

    Page<Game> findAllByOrderByStartDateDesc(Pageable pageable);

    Page<Game> findAllByStatusOrderByStartDateDesc(Game.GameStatus status, Pageable pageable);
}
