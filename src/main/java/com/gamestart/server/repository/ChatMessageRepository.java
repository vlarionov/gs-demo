package com.gamestart.server.repository;

import com.gamestart.server.model.chat.ChatMessage;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ChatMessageRepository extends EntityRepository<ChatMessage> {
    List<ChatMessage> findAllByOrderByDateDesc(Pageable pageable);
}
