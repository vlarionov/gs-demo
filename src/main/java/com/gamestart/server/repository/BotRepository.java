package com.gamestart.server.repository;

import com.gamestart.server.model.bots.Bot;

public interface BotRepository extends EntityRepository<Bot> {
    Bot findOneByLogin(String login);
}
