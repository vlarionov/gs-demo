package com.gamestart.server.repository;

import com.gamestart.server.model.user.User;

//@RepositoryRestResource
public interface UserRepository extends EntityRepository<User> {
    User findOneBySteamInfo_SteamId(String steamId);

}
