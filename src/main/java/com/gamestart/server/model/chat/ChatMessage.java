package com.gamestart.server.model.chat;

import com.gamestart.server.model.AbstractEntity;
import com.gamestart.server.model.user.User;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import java.util.Date;

@Entity
public class ChatMessage extends AbstractEntity {
    private Date date;
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    private String text;

    public ChatMessage() {
    }

    public ChatMessage(Long id, Date date, User user, String text) {
        super(id);
        this.date = date;
        this.user = user;
        this.text = text;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
