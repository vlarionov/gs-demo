package com.gamestart.server.model;

public enum SteamGame {
    DOTA2("570", "Dota 2"), CSGO("730", "CS GO");

    private final String appId;

    private final String name;

    SteamGame(String appId, String name) {
        this.appId = appId;
        this.name = name;
    }

    public static SteamGame findByAppId(String appId) {
        for (SteamGame steamGame : SteamGame.values()) {
            if (steamGame.getAppId().equals(appId)) {
                return steamGame;
            }
        }
        return null;
    }

    public String getAppId() {
        return appId;
    }

    public String getName() {
        return name;
    }

}
