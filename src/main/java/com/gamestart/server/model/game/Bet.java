package com.gamestart.server.model.game;

import com.gamestart.server.model.AbstractEntity;
import com.gamestart.server.model.user.User;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
public class Bet extends AbstractEntity {
    @NotNull
    private Date date;

    @ManyToOne(fetch = FetchType.EAGER)
    @NotNull
    private Game game;

    @NotNull
    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @NotNull
    private Type type;

    @Min(0)
    private int amount;

    private boolean win;

    private int profit;

    public Bet() {
    }

    public Bet(Long id, Date date, Game game, User user, Type type, int amount, boolean win, int profit) {
        super(id);
        this.date = date;
        this.game = game;
        this.user = user;
        this.type = type;
        this.amount = amount;
        this.win = win;
        this.profit = profit;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Game getGame() {
        return game;
    }

    public void setGame(Game game) {
        this.game = game;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public boolean isWin() {
        return win;
    }

    public void setWin(boolean win) {
        this.win = win;
    }

    public int getProfit() {
        return profit;
    }

    public void setProfit(int profit) {
        this.profit = profit;
    }

    public enum Type {
        BLACK, RED, ZERO
    }
}
