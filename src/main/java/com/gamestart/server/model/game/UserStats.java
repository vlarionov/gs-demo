package com.gamestart.server.model.game;

import com.gamestart.server.model.user.User;

public class UserStats implements Comparable<UserStats> {
    private User user;

    private int betsCount;

    private int totalProfit;

    public UserStats() {
    }

    public UserStats(User user, int betsCount, int totalProfit) {
        this.user = user;
        this.betsCount = betsCount;
        this.totalProfit = totalProfit;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getBetsCount() {
        return betsCount;
    }

    public void setBetsCount(int betsCount) {
        this.betsCount = betsCount;
    }

    public int getTotalProfit() {
        return totalProfit;
    }

    public void setTotalProfit(int totalProfit) {
        this.totalProfit = totalProfit;
    }

    @Override
    public int compareTo(UserStats other) {
        return totalProfit - other.totalProfit;
    }
}
