package com.gamestart.server.model.game;

import com.gamestart.server.model.AbstractEntity;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(
        name = "game",
        indexes = {
                @Index(columnList = "startDate")
        }
)
public class Game extends AbstractEntity {
    @Min(1)
    private int number;

    @NotNull
    private GameStatus status;

    //in milliseconds
    @Min(0)
    private long bettingTime;
    @Min(0)
    private long drawingTime;
    @Min(0)
    private long closingTime;

    @NotNull
    private Date startDate;
    private Date finishDate;

    private int minBet;
    private int maxBet;
    private int maxBetCount;
    //TODO
    //@NotNull
    @OneToMany(
            fetch = FetchType.EAGER,
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Bet> bets;

    private double roundNumber;
    private String roundHash;
    private Integer winNumber;

    public Game() {
    }

    public Game(Long id, int number, GameStatus status, long bettingTime, long drawingTime, long closingTime, Date startDate, Date finishDate, int minBet, int maxBet, int maxBetCount, List<Bet> bets, double roundNumber, String roundHash, Integer winNumber) {
        super(id);
        this.number = number;
        this.status = status;
        this.bettingTime = bettingTime;
        this.drawingTime = drawingTime;
        this.closingTime = closingTime;
        this.startDate = startDate;
        this.finishDate = finishDate;
        this.minBet = minBet;
        this.maxBet = maxBet;
        this.maxBetCount = maxBetCount;
        this.bets = bets;
        this.roundNumber = roundNumber;
        this.roundHash = roundHash;
        this.winNumber = winNumber;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public GameStatus getStatus() {
        return status;
    }

    public void setStatus(GameStatus status) {
        this.status = status;
    }

    public long getBettingTime() {
        return bettingTime;
    }

    public void setBettingTime(long bettingTime) {
        this.bettingTime = bettingTime;
    }

    public long getDrawingTime() {
        return drawingTime;
    }

    public void setDrawingTime(long drawingTime) {
        this.drawingTime = drawingTime;
    }

    public long getClosingTime() {
        return closingTime;
    }

    public void setClosingTime(long closingTime) {
        this.closingTime = closingTime;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getFinishDate() {
        return finishDate;
    }

    public void setFinishDate(Date finishDate) {
        this.finishDate = finishDate;
    }

    public int getMinBet() {
        return minBet;
    }

    public void setMinBet(int minBet) {
        this.minBet = minBet;
    }

    public int getMaxBet() {
        return maxBet;
    }

    public void setMaxBet(int maxBet) {
        this.maxBet = maxBet;
    }

    public int getMaxBetCount() {
        return maxBetCount;
    }

    public void setMaxBetCount(int maxBetCount) {
        this.maxBetCount = maxBetCount;
    }

    public List<Bet> getBets() {
        return bets;
    }

    public void setBets(List<Bet> bets) {
        this.bets = bets;
    }

    public double getRoundNumber() {
        return roundNumber;
    }

    public void setRoundNumber(double roundNumber) {
        this.roundNumber = roundNumber;
    }

    public String getRoundHash() {
        return roundHash;
    }

    public void setRoundHash(String roundHash) {
        this.roundHash = roundHash;
    }

    public Integer getWinNumber() {
        return winNumber;
    }

    public void setWinNumber(Integer winNumber) {
        this.winNumber = winNumber;
    }

    public enum GameStatus {
        WAITING, ACTIVE, DRAWING, CLOSING, CLOSED
    }
}