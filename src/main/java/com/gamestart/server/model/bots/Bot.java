package com.gamestart.server.model.bots;

import com.gamestart.server.model.AbstractEntity;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Entity;

@Entity
public class Bot extends AbstractEntity {
    @NotBlank
    private String steamId;

    @NotBlank
    private String login;

    @NotBlank
    private String password;

    @NotBlank
    private String sharedSecret;

    @NotBlank
    private String identitySecret;

    @NotBlank
    @URL
    private String tradeUrl;

    public Bot() {
    }

    public Bot(Long id, String steamId, String login, String password, String sharedSecret, String identitySecret, String tradeUrl) {
        super(id);
        this.steamId = steamId;
        this.login = login;
        this.password = password;
        this.sharedSecret = sharedSecret;
        this.identitySecret = identitySecret;
        this.tradeUrl = tradeUrl;
    }

    public String getSteamId() {
        return steamId;
    }

    public void setSteamId(String steamId) {
        this.steamId = steamId;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSharedSecret() {
        return sharedSecret;
    }

    public void setSharedSecret(String sharedSecret) {
        this.sharedSecret = sharedSecret;
    }

    public String getIdentitySecret() {
        return identitySecret;
    }

    public void setIdentitySecret(String identitySecret) {
        this.identitySecret = identitySecret;
    }

    public String getTradeUrl() {
        return tradeUrl;
    }

    public void setTradeUrl(String tradeUrl) {
        this.tradeUrl = tradeUrl;
    }
}
