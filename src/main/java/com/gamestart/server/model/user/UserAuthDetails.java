package com.gamestart.server.model.user;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.gamestart.server.model.AbstractEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.validation.Valid;
import java.util.Collection;
import java.util.Set;

@Entity
public class UserAuthDetails extends AbstractEntity implements UserDetails {
    //@NotNull
    @Valid
    @ManyToMany(fetch = FetchType.EAGER)
    private Set<Authority> authorities;

    private boolean enabled;

    private boolean accountNonLocked;

    //TODO remove, invalid update
    //@NotNull
    @JsonManagedReference
    @OneToOne(
            targetEntity = User.class,
            mappedBy = "userAuthDetails",
            fetch = FetchType.EAGER
    )
    private User user;

    public UserAuthDetails() {
    }

    public UserAuthDetails(Long id, Set<Authority> authorities, boolean enabled, boolean accountNonLocked, User user) {
        super(id);
        this.authorities = authorities;
        this.enabled = enabled;
        this.accountNonLocked = accountNonLocked;
        this.user = user;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public void setAuthorities(Set<Authority> authorities) {
        this.authorities = authorities;
    }

    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public boolean isAccountNonLocked() {
        return this.accountNonLocked;
    }

    public void setAccountNonLocked(boolean accountNonLocked) {
        this.accountNonLocked = accountNonLocked;
    }

    @Override
    public String getUsername() {
        return user != null ? user.getName() : null;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public String getPassword() {
        return null;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }

}
