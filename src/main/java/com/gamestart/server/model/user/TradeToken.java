package com.gamestart.server.model.user;

import com.google.common.base.Splitter;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Embeddable;
import java.util.Map;

@Embeddable
public class TradeToken {
    @NotBlank
    @URL
    private String url;

    @NotBlank
    private String partnerId;

    @NotBlank
    private String token;

    public TradeToken() {
    }

    public TradeToken(String url, String partnerId, String token) {
        this.url = url;
        this.partnerId = partnerId;
        this.token = token;
    }

    public TradeToken(String url) {
        this.url = url;
        String splitUrl = url.split("\\?")[1];
        Map<String, String> params = Splitter.on('&').trimResults().withKeyValueSeparator("=").split(splitUrl);
        this.partnerId = params.get("partner");
        this.token = params.get("token");
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPartnerId() {
        return partnerId;
    }

    public void setPartnerId(String partnerId) {
        this.partnerId = partnerId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
