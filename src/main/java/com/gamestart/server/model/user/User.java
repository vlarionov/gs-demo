package com.gamestart.server.model.user;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.gamestart.server.model.AbstractEntity;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(name = "users")
public class User extends AbstractEntity {
    @NotBlank
    private String name;

    @NotNull
    private Date registrationDate;

    @Min(0)
    private double balance;

    @NotNull
    @Valid
    private SteamInfo steamInfo;

    @Valid
    private TradeToken tradeToken;

    @NotNull
    @Valid
    @OneToOne(
            cascade = CascadeType.ALL,
            orphanRemoval = true,
            fetch = FetchType.EAGER
    )
    @JoinColumn
    @JsonBackReference
    private UserAuthDetails userAuthDetails;


    public User() {
    }

    public User(Long id, String name, Date registrationDate, double balance, SteamInfo steamInfo, TradeToken tradeToken, UserAuthDetails userAuthDetails) {
        super(id);
        this.name = name;
        this.registrationDate = registrationDate;
        this.balance = balance;
        this.steamInfo = steamInfo;
        this.tradeToken = tradeToken;
        this.userAuthDetails = userAuthDetails;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setRegistrationDate(Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public SteamInfo getSteamInfo() {
        return steamInfo;
    }

    public void setSteamInfo(SteamInfo steamInfo) {
        this.steamInfo = steamInfo;
    }

    public TradeToken getTradeToken() {
        return tradeToken;
    }

    public void setTradeToken(TradeToken tradeToken) {
        this.tradeToken = tradeToken;
    }

    public UserAuthDetails getUserAuthDetails() {
        return userAuthDetails;
    }

    public void setUserAuthDetails(UserAuthDetails userAuthDetails) {
        this.userAuthDetails = userAuthDetails;
    }
}
