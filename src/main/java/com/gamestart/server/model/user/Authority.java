package com.gamestart.server.model.user;

import com.gamestart.server.model.AbstractEntity;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.security.core.GrantedAuthority;

import javax.persistence.Entity;

@Entity
public class Authority extends AbstractEntity implements GrantedAuthority {
    @NotBlank
    private String role;

    public Authority() {
    }

    public Authority(Long id, String role) {
        super(id);
        this.role = role;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String getAuthority() {
        return role;
    }

}
