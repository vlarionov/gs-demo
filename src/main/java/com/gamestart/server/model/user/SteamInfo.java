package com.gamestart.server.model.user;

import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;

import javax.persistence.Embeddable;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Embeddable
@Table(
        indexes = {
                @Index(columnList = "steamId", unique = true),
                @Index(columnList = "steamName", unique = true)
        }
)
public class SteamInfo {
    @NotBlank
    private String steamId;

    @NotBlank
    private String steamName;

    @NotNull
    @URL
    private String profileUrl;

    @NotNull
    @URL
    private String avatarMinUrl;

    @NotNull
    @URL
    private String avatarMediumUrl;

    @NotNull
    @URL
    private String avatarMaxUrl;


    public SteamInfo() {
    }

    public SteamInfo(String steamId, String steamName, String profileUrl, String avatarMinUrl, String avatarMediumUrl, String avatarMaxUrl) {
        this.steamId = steamId;
        this.steamName = steamName;
        this.profileUrl = profileUrl;
        this.avatarMinUrl = avatarMinUrl;
        this.avatarMediumUrl = avatarMediumUrl;
        this.avatarMaxUrl = avatarMaxUrl;
    }

    public String getSteamId() {
        return steamId;
    }

    public void setSteamId(String steamId) {
        this.steamId = steamId;
    }

    public String getSteamName() {
        return steamName;
    }

    public void setSteamName(String steamName) {
        this.steamName = steamName;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getAvatarMinUrl() {
        return avatarMinUrl;
    }

    public void setAvatarMinUrl(String avatarMinUrl) {
        this.avatarMinUrl = avatarMinUrl;
    }

    public String getAvatarMediumUrl() {
        return avatarMediumUrl;
    }

    public void setAvatarMediumUrl(String avatarMediumUrl) {
        this.avatarMediumUrl = avatarMediumUrl;
    }

    public String getAvatarMaxUrl() {
        return avatarMaxUrl;
    }

    public void setAvatarMaxUrl(String avatarMaxUrl) {
        this.avatarMaxUrl = avatarMaxUrl;
    }
}
