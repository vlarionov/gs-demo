package com.gamestart.server.model.item;

import com.gamestart.server.model.AbstractEntity;
import com.gamestart.server.model.SteamGame;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Entity
@Table(
        name = "item_market_info",
        uniqueConstraints = {
                @UniqueConstraint(columnNames = {
                        "steamGame",
                        "marketHashName"
                })
        },
        indexes = {
                @Index(columnList = "steamGame,marketHashName", unique = true)
        }
)
public class ItemMarketInfo extends AbstractEntity {
    @NotNull
    private SteamGame steamGame;

    @NotBlank
    private String marketHashName;

    @Min(0)
    private double price;

    @Min(0)
    private int volume;

    @NotNull
    private Date updated;

    public ItemMarketInfo() {
    }

    public ItemMarketInfo(Long id, SteamGame steamGame, String marketHashName, double price, int volume, Date updated) {
        super(id);
        this.steamGame = steamGame;
        this.marketHashName = marketHashName;
        this.price = price;
        this.volume = volume;
        this.updated = updated;
    }

    public SteamGame getSteamGame() {
        return steamGame;
    }

    public void setSteamGame(SteamGame steamGame) {
        this.steamGame = steamGame;
    }

    public String getMarketHashName() {
        return marketHashName;
    }

    public void setMarketHashName(String marketHashName) {
        this.marketHashName = marketHashName;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getVolume() {
        return volume;
    }

    public void setVolume(int volume) {
        this.volume = volume;
    }

    public Date getUpdated() {
        return updated;
    }

    public void setUpdated(Date updated) {
        this.updated = updated;
    }
}
