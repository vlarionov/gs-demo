package com.gamestart.server.model.item;

import com.gamestart.server.model.AbstractEntity;
import com.gamestart.server.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Table(
        indexes = {
                @Index(columnList = "status"),
                @Index(columnList = "offerId", unique = true)
        }
)
public class TradeOperation extends AbstractEntity {
    @NotNull
    private Type type;

    @NotNull
    private Date date;

    @NotNull
    @ManyToOne(
            fetch = FetchType.EAGER
    )
    private User user;

    @ElementCollection(
            fetch = FetchType.EAGER
    )
    private List<SteamItem> items;

    @NotNull
    private Status status;

    private String offerId;

    public TradeOperation() {
    }

    public TradeOperation(Long id, Type type, Date date, User user, List<SteamItem> items, Status status, String offerId) {
        super(id);
        this.type = type;
        this.date = date;
        this.user = user;
        this.items = items;
        this.status = status;
        this.offerId = offerId;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<SteamItem> getItems() {
        return items;
    }

    public void setItems(List<SteamItem> items) {
        this.items = items;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public String getOfferId() {
        return offerId;
    }

    public void setOfferId(String offerId) {
        this.offerId = offerId;
    }

    public enum Type {
        SELL, BUY
    }

    public enum Status {
        CREATED,

        PROCESSED,

        ACTIVE,

        COMPLETE,

        DECLINED,

        INVALID_ITEMS_ERROR,

        ERROR
    }

}
