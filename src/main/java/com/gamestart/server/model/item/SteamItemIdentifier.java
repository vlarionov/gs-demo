package com.gamestart.server.model.item;

import com.gamestart.server.model.SteamGame;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;

@Embeddable
public class SteamItemIdentifier {
    @NotNull
    private SteamGame steamGame;
    @NotEmpty
    private String contextId;
    @NotEmpty
    private String assetId;

    public SteamItemIdentifier() {
    }

    public SteamItemIdentifier(SteamGame steamGame, String contextId, String assetId) {
        this.steamGame = steamGame;
        this.contextId = contextId;
        this.assetId = assetId;
    }

    public SteamGame getSteamGame() {
        return steamGame;
    }

    public void setSteamGame(SteamGame steamGame) {
        this.steamGame = steamGame;
    }

    public String getContextId() {
        return contextId;
    }

    public void setContextId(String contextId) {
        this.contextId = contextId;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SteamItemIdentifier)) return false;

        SteamItemIdentifier that = (SteamItemIdentifier) o;

        if (steamGame != that.steamGame) return false;
        if (!contextId.equals(that.contextId)) return false;
        return assetId.equals(that.assetId);

    }

    @Override
    public int hashCode() {
        int result = steamGame.hashCode();
        result = 31 * result + contextId.hashCode();
        result = 31 * result + assetId.hashCode();
        return result;
    }
}
