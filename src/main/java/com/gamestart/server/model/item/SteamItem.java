package com.gamestart.server.model.item;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.Embeddable;
import javax.persistence.Transient;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.HashMap;
import java.util.Map;

@Embeddable
public class SteamItem {
    @NotNull
    private SteamItemIdentifier identifier;

    @NotEmpty
    private String classId;
    @NotEmpty
    private String instanceId;
    @NotEmpty
    private String marketHashName;

    @NotEmpty
    private String name;
    @NotEmpty
    private String iconUrl;

    @Min(1)
    private int amount;

    @Min(1)
    private double price;

    //TODO
    @Transient
    private Map<String, String> tags = new HashMap<>();

    public SteamItem() {
    }

    public SteamItem(SteamItemIdentifier identifier, String classId, String instanceId, String marketHashName, String name, String iconUrl, int amount, double price, Map<String, String> tags) {
        this.identifier = identifier;
        this.classId = classId;
        this.instanceId = instanceId;
        this.marketHashName = marketHashName;
        this.name = name;
        this.iconUrl = iconUrl;
        this.amount = amount;
        this.price = price;
        this.tags = tags;
    }

    public SteamItemIdentifier getIdentifier() {
        return identifier;
    }

    public void setIdentifier(SteamItemIdentifier identifier) {
        this.identifier = identifier;
    }

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(String instanceId) {
        this.instanceId = instanceId;
    }

    public String getMarketHashName() {
        return marketHashName;
    }

    public void setMarketHashName(String marketHashName) {
        this.marketHashName = marketHashName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIconUrl() {
        return iconUrl;
    }

    public void setIconUrl(String iconUrl) {
        this.iconUrl = iconUrl;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Map<String, String> getTags() {
        return tags;
    }

    public void setTags(Map<String, String> tags) {
        this.tags = tags;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SteamItem)) return false;

        SteamItem steamItem = (SteamItem) o;

        return identifier.equals(steamItem.identifier);

    }

    @Override
    public int hashCode() {
        return identifier.hashCode();
    }
}
