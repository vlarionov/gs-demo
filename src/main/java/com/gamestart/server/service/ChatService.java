package com.gamestart.server.service;

import com.gamestart.server.model.chat.ChatMessage;

import java.util.List;

public interface ChatService {
    void sendMessage(Long userId, String text);

    List<ChatMessage> getMessages(int pageNumber, int pageSize);
}
