package com.gamestart.server.service;

import com.gamestart.server.model.SteamGame;
import com.gamestart.server.model.item.SteamItem;
import com.gamestart.server.model.item.SteamItemIdentifier;
import com.gamestart.server.model.item.TradeOperation;
import com.gamestart.server.model.user.User;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Set;

public interface TradeService {
    TradeOperation sellItems(User user, List<SteamItemIdentifier> itemsIdentifies);

    Set<String> getCategories(SteamGame steamGame);

    Set<String> getTags(SteamGame steamGame, String category);

    List<SteamItem> getStoreItems(SteamGame steamGame);

    TradeOperation buyItem(User user, SteamItemIdentifier itemIdentifier);

    void checkTradeOperations();

    void setOperationOfferId(Long operationId, String offerId);

    void changeOperationStatusById(Long operationId, TradeOperation.Status status);

    void changeOperationStatusByOfferId(String offerId, TradeOperation.Status status);

    Page<TradeOperation> getTradeOperationsHistory(
            Long userId,
            TradeOperation.Type type,
            int pageNumber,
            int pageSize
    );
}
