package com.gamestart.server.service;

public interface UserService {

    void saveTradeToken(Long userId, String tradeUrl);

    void changeBalanceOnAmount(Long userId, double amount);

}
