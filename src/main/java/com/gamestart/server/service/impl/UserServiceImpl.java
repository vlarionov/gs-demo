package com.gamestart.server.service.impl;

import com.gamestart.server.exception.NotEnoughBalanceException;
import com.gamestart.server.model.user.SteamInfo;
import com.gamestart.server.model.user.TradeToken;
import com.gamestart.server.model.user.User;
import com.gamestart.server.model.user.UserAuthDetails;
import com.gamestart.server.repository.AuthorityRepository;
import com.gamestart.server.repository.UserRepository;
import com.gamestart.server.service.SteamService;
import com.gamestart.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.openid.OpenIDAuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Date;

@Service
public class UserServiceImpl implements UserService, AuthenticationUserDetailsService<OpenIDAuthenticationToken> {
    private final UserRepository userRepository;
    private final AuthorityRepository authorityRepository;

    private final SteamService steamService;

    @Autowired
    public UserServiceImpl(
            UserRepository userRepository,
            AuthorityRepository authorityRepository,
            SteamService steamService
    ) {
        this.userRepository = userRepository;
        this.authorityRepository = authorityRepository;
        this.steamService = steamService;
    }

    @Override
    @Transactional
    public UserDetails loadUserDetails(OpenIDAuthenticationToken token) throws UsernameNotFoundException {
        String steamId = parseSteamId((String) token.getPrincipal());
        SteamInfo steamInfo = steamService.getPlayerSummaries(steamId);
        User userFromDb = userRepository.findOneBySteamInfo_SteamId(steamId);
        if (userFromDb != null) {
            userFromDb.setSteamInfo(steamInfo);
            userFromDb = userRepository.save(userFromDb);
        } else {
            UserAuthDetails authDetails = new UserAuthDetails(
                    null,
                    Collections.singleton(
                            authorityRepository.findOneByRole("PLAYER").get()
                    ),
                    true,
                    true,
                    null
            );
            userFromDb = userRepository.save(new User(
                    null,
                    steamInfo.getSteamName(),
                    new Date(),
                    10,
                    steamInfo,
                    null,
                    authDetails
            ));
            userFromDb.getUserAuthDetails().setUser(userFromDb);
            userFromDb = userRepository.save(userFromDb);
        }
        return userFromDb.getUserAuthDetails();
    }

    private String parseSteamId(String steamUserPath) {
        String[] parts = steamUserPath.split("/");
        return parts[parts.length - 1];
    }

    @Override
    @Transactional
    public void saveTradeToken(Long userId, String tradeUrl) {
        TradeToken tradeToken = new TradeToken(tradeUrl);
        User user = userRepository.findOne(userId);
        user.setTradeToken(tradeToken);
        userRepository.save(user);
    }

    @Override
    @Transactional
    public void changeBalanceOnAmount(Long userId, double amount) throws NotEnoughBalanceException {
        User user = userRepository.findOne(userId);
        user.setBalance(user.getBalance() + amount);
        if (user.getBalance() < 0) {
            throw new NotEnoughBalanceException();
        }
        userRepository.save(user);
    }

}
