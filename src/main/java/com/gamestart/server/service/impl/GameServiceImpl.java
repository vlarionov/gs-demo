package com.gamestart.server.service.impl;

import com.gamestart.server.exception.GameClosedException;
import com.gamestart.server.exception.InvalidBetException;
import com.gamestart.server.exception.MaxBetCountException;
import com.gamestart.server.model.game.Bet;
import com.gamestart.server.model.game.Game;
import com.gamestart.server.model.game.UserStats;
import com.gamestart.server.model.user.User;
import com.gamestart.server.repository.BetRepository;
import com.gamestart.server.repository.GameRepository;
import com.gamestart.server.repository.UserRepository;
import com.gamestart.server.service.GameService;
import com.gamestart.server.service.UserService;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Service
public class GameServiceImpl implements GameService {
    private static final Logger LOG = LoggerFactory.getLogger(GameServiceImpl.class);

    private static final long GAME_STATUS_REFRESH_DELAY = 333L;

    private final Lock gameLock = new ReentrantLock();

    private final Random random = new Random();

    private final GameRepository gameRepository;
    private final BetRepository betRepository;
    private final UserRepository userRepository;

    private final UserService userService;

    @Autowired
    public GameServiceImpl(
            GameRepository gameRepository,
            BetRepository betRepository,
            UserRepository userRepository,
            UserService userService
    ) {
        this.gameRepository = gameRepository;
        this.betRepository = betRepository;
        this.userRepository = userRepository;
        this.userService = userService;
    }

    @Override
    public Game findFinishedGame(Long gameId) {
        return gameRepository.findOneByIdAndFinishDateIsNotNull(gameId);
    }

    @Override
    public Page<Game> getHistory(int pageNumber, int pageSize) {
        return gameRepository.findAllByStatusOrderByStartDateDesc(
                Game.GameStatus.CLOSED,
                new PageRequest(pageNumber, pageSize)
        );
    }

    @Override
    public Page<Bet> getUserBets(Long userId, int pageNumber, int pageSize) {
        return betRepository.findAllByUser_IdOrderByDateDesc(
                userId,
                new PageRequest(pageNumber, pageSize)
        );
    }

    @Override
    @Transactional(readOnly = true)
    public Game getCurrent() {
        Optional<Game> game = gameRepository.findAllByOrderByStartDateDesc(
                new PageRequest(0, 1)
        ).getContent().stream().findFirst();
        if (game.isPresent()) {
            return game.get();
        } else {
            return null;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<Bet> getBets(Long gameId) {
        Game game = gameRepository.findOne(gameId);
        return game != null ? game.getBets() : null;
    }

    @Override
    @Cacheable("userStats")
    @Transactional(readOnly = true)
    public UserStats getUserStats(Long userId) {
        List<Bet> userBets = betRepository.findAllByUser_Id(userId);
        return new UserStats(
                userRepository.findOne(userId),
                userBets.size(),
                userBets.stream()
                        .filter(Bet::isWin)
                        .mapToInt(Bet::getProfit)
                        .sum()
        );
    }

    @Override
    @Cacheable("topUsersStats")
    @Transactional(readOnly = true)
    public List<UserStats> geTopUsersStats() {
        List<UserStats> topUsersStats = new ArrayList<>();
        int page = 0;
        List<User> users;
        do {
            users = userRepository.findAll(
                    new PageRequest(page++, 1000)
            ).getContent();
            List<UserStats> userStatsList = users.stream()
                    .map(user -> getUserStats(user.getId()))
                    .sorted()
                    .collect(Collectors.toList());
            topUsersStats = CollectionUtils.collate(topUsersStats, userStatsList).stream()
                    .limit(100)
                    .collect(Collectors.toList());
        } while (!users.isEmpty());
        return topUsersStats;
    }


    @Override
    @Scheduled(fixedRate = GAME_STATUS_REFRESH_DELAY)
    public void refreshStatus() {
        try {
            gameLock.lock();
            refreshStatusWithoutLocking();
        } finally {
            gameLock.unlock();
        }
    }

    @Transactional
    private void refreshStatusWithoutLocking() {
        Game currentGame = getCurrent();
        if (currentGame == null) {
            create(1);
            return;
        }

        if (currentGame.getStatus() == Game.GameStatus.WAITING) {
            return;
        }

        if (getTimeToNewStatus(currentGame) > 0) {
            return;
        }

        switch (currentGame.getStatus()) {
            case ACTIVE:
                int winNumber = ((int) (15 * currentGame.getRoundNumber())) + currentGame.getBets().size();
                while (winNumber >= 15) {
                    winNumber -= 15;
                }
                currentGame.setWinNumber(winNumber);
                currentGame.setStatus(Game.GameStatus.DRAWING);
                break;
            case DRAWING:
                for (Bet bet : currentGame.getBets()) {
                    closeBet(bet, currentGame.getWinNumber());
                }
                currentGame.setStatus(Game.GameStatus.CLOSING);
                break;
            case CLOSING:
                currentGame.setFinishDate(new Date());
                currentGame.setStatus(Game.GameStatus.CLOSED);
                create(currentGame.getNumber() + 1);
                break;
        }
        gameRepository.save(currentGame);
    }

    @Override
    public Long getTimeToNewStatus(Game game) {
        Optional<Bet> firstBetOptional = findFirstBet(game);
        if (!firstBetOptional.isPresent()) {
            return null;
        }
        Instant now = new Date().toInstant();

        Bet firstBet = firstBetOptional.get();
        Instant expiryDate = firstBet.getDate().toInstant();

        expiryDate = expiryDate.plus(game.getBettingTime(), ChronoUnit.MILLIS);
        if (game.getStatus() != Game.GameStatus.ACTIVE) {
            expiryDate = expiryDate.plus(game.getDrawingTime(), ChronoUnit.MILLIS);
            if (game.getStatus() != Game.GameStatus.DRAWING) {
                expiryDate = expiryDate.plus(game.getClosingTime(), ChronoUnit.MILLIS);
            }
        }
        return expiryDate.toEpochMilli() - now.toEpochMilli();
    }

    @Override
    public void createBet(Long gameId, Long userId, int amount, Bet.Type type) {
        try {
            gameLock.lock();
            createBetWithoutLocking(gameId, userId, amount, type);
        } finally {
            gameLock.unlock();
        }
    }

    @Transactional
    private void createBetWithoutLocking(Long gameId, Long userId, int amount, Bet.Type type) {
        Game currentGame = gameRepository.findOne(gameId);
        if (currentGame == null) {
            throw new GameClosedException();
        }

        boolean isValidStatus = currentGame.getStatus() == Game.GameStatus.WAITING;
        isValidStatus = isValidStatus || currentGame.getStatus() == Game.GameStatus.ACTIVE;
        if (!isValidStatus) {
            throw new GameClosedException();
        }

        userService.changeBalanceOnAmount(userId, -amount);

        if (currentGame.getBets().size() >= currentGame.getMaxBetCount()) {
            throw new MaxBetCountException();
        }

        if (amount < currentGame.getMinBet() || amount > currentGame.getMaxBet()) {
            throw new InvalidBetException();
        }

        currentGame.getBets().add(new Bet(
                null,
                new Date(),
                currentGame,
                userRepository.findOne(userId),
                type,
                amount,
                false,
                0
        ));
        if (currentGame.getBets().size() == 1) {
            currentGame.setStatus(Game.GameStatus.ACTIVE);
        }
        gameRepository.save(currentGame);
    }

    private Optional<Bet> findFirstBet(Game game) {
        return game.getBets().stream().findFirst();
    }

    private void closeBet(Bet bet, int winNumber) {
        boolean isWin = false;
        int winMultiplier;
        Bet.Type betType = bet.getType();
        if (betType == Bet.Type.ZERO && winNumber == 0) {
            isWin = true;
            winMultiplier = 14;
        } else if ((betType == Bet.Type.RED && (winNumber != 0 && winNumber <= 7))
                || (betType == Bet.Type.BLACK && (winNumber != 0 && winNumber >= 8))) {
            isWin = true;
            winMultiplier = 2;
        } else {
            winMultiplier = 0;
        }
        bet.setWin(isWin);
        bet.setProfit(bet.getAmount() * winMultiplier);
        betRepository.save(bet);
        userService.changeBalanceOnAmount(
                bet.getUser().getId(),
                bet.getProfit()
        );
    }

    private void create(int number) {
        double roundNumber = random.nextDouble();
        gameRepository.save(new Game(
                null,
                number,
                Game.GameStatus.WAITING,
                60000,
                15000,
                5000,
                new Date(),
                null,
                1,
                10,
                100,
                Collections.emptyList(),
                roundNumber,
                DigestUtils.sha256Hex(String.valueOf(roundNumber)),
                20000
        ));
    }

}