package com.gamestart.server.service.impl;

import com.gamestart.server.controller.websocket.BotController;
import com.gamestart.server.model.SteamGame;
import com.gamestart.server.model.bots.Bot;
import com.gamestart.server.model.item.SteamItem;
import com.gamestart.server.model.item.SteamItemIdentifier;
import com.gamestart.server.model.item.TradeOperation;
import com.gamestart.server.model.user.User;
import com.gamestart.server.repository.BotRepository;
import com.gamestart.server.repository.TradeOperationRepository;
import com.gamestart.server.service.SteamService;
import com.gamestart.server.service.TradeService;
import com.gamestart.server.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class TradeServiceImpl implements TradeService {
    private static final Logger LOG = LoggerFactory.getLogger(TradeServiceImpl.class);

    private final BotRepository botRepository;
    private final TradeOperationRepository tradeOperationRepository;

    private final SteamService steamService;
    private final UserService userService;

    private final BotController botController;


    @Autowired
    public TradeServiceImpl(
            BotRepository botRepository,
            TradeOperationRepository tradeOperationRepository,
            SteamService steamService,
            UserService userService,
            @Lazy BotController botController
    ) {
        this.botRepository = botRepository;
        this.tradeOperationRepository = tradeOperationRepository;
        this.steamService = steamService;
        this.userService = userService;
        this.botController = botController;
    }


    //region sellItems
    @Override
    @Transactional
    public TradeOperation sellItems(User user, List<SteamItemIdentifier> itemsIdentifies) {
        Map<SteamItemIdentifier, SteamItem> inventory = new HashMap<>();
        for (SteamGame steamGame : SteamGame.values()) {
            try {
                steamService.getInventory(
                        user.getSteamInfo().getSteamId(),
                        steamGame
                ).forEach(item -> {
                    inventory.put(
                            item.getIdentifier(),
                            item
                    );
                });
            } catch (Exception ignored) {
            }
        }
        List<SteamItem> steamItems = new ArrayList<>();
        itemsIdentifies.forEach(itemIdentifier -> {
            SteamItem item = inventory.get(itemIdentifier);
            if (item != null) {
                steamItems.add(item);
            }
        });

        TradeOperation.Status status = TradeOperation.Status.CREATED;
        if (itemsIdentifies.size() != steamItems.size()) {
            status = TradeOperation.Status.INVALID_ITEMS_ERROR;
        }

        return tradeOperationRepository.save(new TradeOperation(
                null,
                TradeOperation.Type.SELL,
                new Date(),
                user,
                steamItems,
                status,
                null
        ));
    }
    //endregion

    //region buyItems
    @Override
    @Cacheable("categories")
    public Set<String> getCategories(SteamGame steamGame) {
        Bot bot = botRepository.findAll().stream().findAny().get();
        List<SteamItem> botInventory = steamService.getInventory(
                bot.getSteamId(),
                steamGame
        );
        return botInventory.stream()
                .flatMap(item -> item.getTags().keySet().stream())
                .collect(Collectors.toSet());
    }

    @Override
    @Cacheable("tags")
    public Set<String> getTags(SteamGame steamGame, String category) {
        Set<String> result = new HashSet<>();
        Bot bot = botRepository.findAll().stream().findAny().get();
        List<SteamItem> botInventory = steamService.getInventory(
                bot.getSteamId(),
                steamGame
        );
        botInventory.forEach(item -> {
            String tag = item.getTags().get(category);
            if (tag != null) {
                result.add(tag);
            }
        });
        return result;
    }

    @Override
    @Transactional(readOnly = true)
    public List<SteamItem> getStoreItems(SteamGame steamGame) {
        Bot bot = botRepository.findAll().stream().findAny().get();
        return steamService.getInventory(
                bot.getSteamId(),
                steamGame
        );
    }

    @Override
    @Transactional
    public TradeOperation buyItem(User user, SteamItemIdentifier itemIdentifier) {
        Optional<SteamItem> foundItemOptional = getStoreItems(itemIdentifier.getSteamGame()).stream()
                .filter(item -> item.getIdentifier().equals(itemIdentifier))
                .findAny();

        TradeOperation.Status status = TradeOperation.Status.CREATED;

        List<SteamItem> foundItems = new ArrayList<>();
        if (foundItemOptional.isPresent()) {
            SteamItem foundItem = foundItemOptional.get();
            userService.changeBalanceOnAmount(
                    user.getId(),
                    -foundItem.getPrice() * foundItem.getAmount()
            );
            foundItems.add(foundItem);
        } else {
            status = TradeOperation.Status.INVALID_ITEMS_ERROR;
        }

        return tradeOperationRepository.save(new TradeOperation(
                null,
                TradeOperation.Type.BUY,
                new Date(),
                user,
                foundItems,
                status,
                null
        ));
    }
    //endregion


    @Override
    @Scheduled(fixedDelay = 1000L)
    @Transactional
    public void checkTradeOperations() {
        tradeOperationRepository.findAllCreated().forEach(
                operation -> {
                    List<SteamItemIdentifier> itemsToGive;
                    List<SteamItemIdentifier> itemsToReceive;

                    List<SteamItemIdentifier> tradeItems = operation.getItems().stream()
                            .map(SteamItem::getIdentifier)
                            .collect(Collectors.toList());
                    if (operation.getType().equals(TradeOperation.Type.SELL)) {
                        itemsToGive = Collections.emptyList();
                        itemsToReceive = tradeItems;
                    } else {
                        itemsToGive = tradeItems;
                        itemsToReceive = Collections.emptyList();
                    }
                    botController.sendOffer(
                            operation.getId(),
                            operation.getUser().getSteamInfo().getSteamId(),
                            operation.getUser().getTradeToken().getToken(),
                            itemsToGive,
                            itemsToReceive
                    );
                    operation.setStatus(
                            TradeOperation.Status.PROCESSED
                    );
                    tradeOperationRepository.save(operation);
                }
        );
    }

    @Override
    @Transactional
    public void setOperationOfferId(Long operationId, String offerId) {
        TradeOperation tradeOperation = tradeOperationRepository.findOne(operationId);
        tradeOperation.setOfferId(offerId);
        tradeOperationRepository.save(tradeOperation);
    }

    @Override
    @Transactional
    public void changeOperationStatusById(Long operationId, TradeOperation.Status status) {
        TradeOperation operation = tradeOperationRepository.findOne(operationId);
        changeOperationStatus(operation, status);
    }

    @Override
    @Transactional
    public void changeOperationStatusByOfferId(String offerId, TradeOperation.Status status) {
        TradeOperation operation = tradeOperationRepository.findOneByOfferId(offerId);
        changeOperationStatus(operation, status);
    }

    private void changeOperationStatus(TradeOperation operation, TradeOperation.Status status) {
        operation.setStatus(status);
        TradeOperation.Type type = operation.getType();
        if ((type == TradeOperation.Type.SELL && status == TradeOperation.Status.COMPLETE) ||
                (type == TradeOperation.Type.BUY && status == TradeOperation.Status.DECLINED)) {
            double itemsCost = operation.getItems().stream()
                    .mapToDouble(item -> item.getPrice() * item.getAmount())
                    .sum();
            userService.changeBalanceOnAmount(
                    operation.getUser().getId(),
                    itemsCost
            );
        }
        tradeOperationRepository.save(operation);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<TradeOperation> getTradeOperationsHistory(
            Long userId,
            TradeOperation.Type type,
            int pageNumber,
            int pageSize
    ) {
        return tradeOperationRepository.findAllByUser_IdAndTypeOrderByDateDesc(
                userId,
                type,
                new PageRequest(pageNumber, pageSize)
        );
    }

}
