package com.gamestart.server.service.impl;

import com.gamestart.server.model.chat.ChatMessage;
import com.gamestart.server.repository.ChatMessageRepository;
import com.gamestart.server.repository.UserRepository;
import com.gamestart.server.service.ChatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class ChatServiceImpl implements ChatService {
    private final ChatMessageRepository chatMessageRepository;
    private final UserRepository userRepository;


    @Autowired
    public ChatServiceImpl(
            ChatMessageRepository chatMessageRepository,
            UserRepository userRepository
    ) {
        this.chatMessageRepository = chatMessageRepository;
        this.userRepository = userRepository;
    }


    @Transactional
    @Override
    public void sendMessage(Long userId, String text) {
        ChatMessage message = new ChatMessage(
                null,
                new Date(),
                userRepository.findOne(userId),
                text
        );
        chatMessageRepository.save(message);
    }

    @Transactional(readOnly = true)
    @Override
    public List<ChatMessage> getMessages(int pageNumber, int pageSize) {
        return chatMessageRepository.findAllByOrderByDateDesc(
                new PageRequest(pageNumber, pageSize)
        );
    }
}
