package com.gamestart.server.service.impl;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.gamestart.server.exception.InventoryNotAvailableException;
import com.gamestart.server.exception.SteamUserNotFoundException;
import com.gamestart.server.model.SteamGame;
import com.gamestart.server.model.item.ItemMarketInfo;
import com.gamestart.server.model.item.SteamItem;
import com.gamestart.server.model.item.SteamItemIdentifier;
import com.gamestart.server.model.user.SteamInfo;
import com.gamestart.server.repository.ItemMarketInfoRepository;
import com.gamestart.server.service.SteamService;
import org.apache.commons.lang3.concurrent.TimedSemaphore;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestOperations;

import java.util.*;
import java.util.concurrent.TimeUnit;

@Service
public class SteamServiceImpl implements SteamService {
    private static final int MINIMAL_MARKET_VOLUME = 100;

    private static final Logger LOG = LoggerFactory.getLogger(SteamServiceImpl.class);

    private final ItemMarketInfoRepository itemMarketInfoRepository;

    private final RestOperations restOperations;

    private final String steamKey;
    private final String playerSummariesUrl;
    private final String inventoryUrl;

    private final String getPricesUrl;

    private final TimedSemaphore steamSemaphore = new TimedSemaphore(1L, TimeUnit.SECONDS, 3);

    private final TimedSemaphore marketSemaphore = new TimedSemaphore(1L, TimeUnit.MINUTES, 1);

    @Autowired
    public SteamServiceImpl(
            ItemMarketInfoRepository itemMarketInfoRepository,
            @Value("${steam.key}") String steamKey,
            @Value("${steam.playerSummariesUrl}") String playerSummariesUrl,
            @Value("${steam.inventoryUrl}") String inventoryUrl,
            @Value("${market.getPricesUrl}") String getPricesUrl,
            RestOperations restOperations
    ) {
        this.itemMarketInfoRepository = itemMarketInfoRepository;
        this.steamKey = steamKey;
        this.playerSummariesUrl = playerSummariesUrl;
        this.inventoryUrl = inventoryUrl;
        this.getPricesUrl = getPricesUrl;
        this.restOperations = restOperations;

    }

    @Override
    public SteamInfo getPlayerSummaries(String steamId) throws SteamUserNotFoundException {
        try {
            steamSemaphore.acquire();
            Map<String, String> params = new HashMap<>();
            params.put("steamKey", steamKey);
            params.put("steamId", steamId);
            JsonNode response = restOperations.getForObject(
                    playerSummariesUrl,
                    JsonNode.class,
                    params
            );
            JsonNode responseNode = response.get("response");
            ArrayNode playersNodes = (ArrayNode) responseNode.get("players");
            if (playersNodes.size() == 0) {
                throw new SteamUserNotFoundException();
            }
            JsonNode playerNode = playersNodes.get(0);
            return new SteamInfo(
                    playerNode.get("steamid").asText(),
                    playerNode.get("personaname").asText(),
                    playerNode.get("profileurl").asText(),
                    playerNode.get("avatar").asText(),
                    playerNode.get("avatarmedium").asText(),
                    playerNode.get("avatarfull").asText()
            );
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }

    }

    @Transactional(readOnly = true)
    @Override
    public ItemMarketInfo getItemMarketInfo(SteamGame steamGame, String marketHashName) {
        return itemMarketInfoRepository.findOneBySteamGameAndMarketHashName(steamGame, marketHashName);
    }

    @Scheduled(fixedRate = 12 * 60 * 60 * 1000L)
    @Transactional
    @Override
    public void updateItemMarketInfo() {
        itemMarketInfoRepository.deleteAll();
        try {
            for (SteamGame steamGame : SteamGame.values()) {
                marketSemaphore.acquire();
                Date now = new Date();
                JsonNode response = restOperations.getForObject(
                        getPricesUrl,
                        JsonNode.class,
                        Collections.singletonMap("appId", steamGame.getAppId())
                ).get("response");
                JsonNode itemsNode = response.get("items");
                if (itemsNode == null) {
                    //todo
                    throw new RuntimeException();
                }
                itemsNode.fields().forEachRemaining(entry -> {
                    JsonNode entryValue = entry.getValue();
                    int volume = entryValue.get("quantity").asInt();
                    if (volume < MINIMAL_MARKET_VOLUME) {
                        return;
                    }
                    if (itemMarketInfoRepository.findOneBySteamGameAndMarketHashName(
                            steamGame,
                            entry.getKey()
                    ) == null) {
                        itemMarketInfoRepository.save(new ItemMarketInfo(
                                null,
                                steamGame,
                                entry.getKey(),
                                entryValue.get("value").asDouble(),
                                volume,
                                now
                        ));
                    }
                });
            }
        } catch (Exception e) {
            LOG.error("Update itemMarketInfo error", e);
        }
    }


    @Cacheable("inventory")
    @Override
    public List<SteamItem> getInventory(String steamId, SteamGame steamGame) {
        try {
            steamSemaphore.acquire();
            Map<String, String> params = new HashMap<>();
            params.put("steamId", steamId);
            params.put("appId", steamGame.getAppId());
            JsonNode response = restOperations.getForObject(
                    inventoryUrl,
                    JsonNode.class,
                    params
            );
            List<SteamItem> items = new ArrayList<>();
            ObjectNode rgInventory = (ObjectNode) response.get("rgInventory");
            ObjectNode rgDescriptions = (ObjectNode) response.get("rgDescriptions");
            if (rgInventory == null) {
                throw new InventoryNotAvailableException();
            }

            rgInventory.forEach(item -> {
                ObjectNode description = extractRgDescription(item, rgDescriptions);
                String appId = description.get("appid").asText();
                if (!steamGame.getAppId().equals(appId)) {
                    return;
                }
                String assetId = item.get("id").asText();
                String classId = item.get("classid").asText();
                String instanceId = item.get("instanceid").asText();
                String marketHashName = description.get("market_hash_name").asText();
                Map<String, String> tags = extractItemTags((ArrayNode) description.get("tags"));

                if (!"1".equals(description.get("marketable").asText())) {
                    return;
                }
                if (!"1".equals(description.get("tradable").asText())) {
                    return;
                }

                ItemMarketInfo itemMarketInfo = getItemMarketInfo(
                        steamGame,
                        marketHashName
                );
                if (itemMarketInfo == null) {
                    return;
                }

                items.add(new SteamItem(
                        new SteamItemIdentifier(
                                steamGame,
                                "2",
                                assetId
                        ),
                        classId,
                        instanceId,
                        marketHashName,
                        description.get("name").asText(),
                        "http://cdn.steamcommunity.com/economy/image/" + description.get("icon_url").asText(),
                        Integer.parseInt(item.get("amount").asText()),
                        itemMarketInfo.getPrice(),
                        tags
                ));
            });
            return items;
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        } catch (Exception e) {
            throw new InventoryNotAvailableException();
        }
    }

    private ObjectNode extractRgDescription(
            JsonNode item,
            ObjectNode rgDescriptions
    ) {
        String hash = String.format(
                "%s_%s",
                item.get("classid").asText(),
                item.get("instanceid").asText()
        );
        return (ObjectNode) rgDescriptions.get(hash);
    }

    private Map<String, String> extractItemTags(ArrayNode tagsNodes) {
        Map<String, String> result = new HashMap<>();
        tagsNodes.forEach(tag -> {
            result.put(
                    tag.get("category_name").asText(),
                    tag.get("name").asText()
            );
        });
        return result;
    }


}
