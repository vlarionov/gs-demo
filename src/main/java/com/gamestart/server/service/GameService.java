package com.gamestart.server.service;

import com.gamestart.server.model.game.Bet;
import com.gamestart.server.model.game.Game;
import com.gamestart.server.model.game.UserStats;
import org.springframework.data.domain.Page;

import java.util.List;

public interface GameService {

    Game findFinishedGame(Long gameId);

    Page<Game> getHistory(int pageNumber, int pageSize);

    Page<Bet> getUserBets(Long userId, int pageNumber, int pageSize);

    Game getCurrent();

    List<Bet> getBets(Long gameId);

    UserStats getUserStats(Long userId);

    List<UserStats> geTopUsersStats();

    void refreshStatus();

    Long getTimeToNewStatus(Game game);

    void createBet(Long gameId, Long userId, int amount, Bet.Type type);

}
