package com.gamestart.server.service;

import com.gamestart.server.model.SteamGame;
import com.gamestart.server.model.item.ItemMarketInfo;
import com.gamestart.server.model.item.SteamItem;
import com.gamestart.server.model.user.SteamInfo;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface SteamService {
    SteamInfo getPlayerSummaries(String steamId);

    ItemMarketInfo getItemMarketInfo(SteamGame steamGame, String marketHashName);

    @Scheduled(fixedRate = 12 * 60 * 60 * 1000L)
    @Transactional
    void updateItemMarketInfo();

    List<SteamItem> getInventory(String steamId, SteamGame steamGame);

}
