package com.gamestart.server.config;

import org.openid4java.consumer.ConsumerException;
import org.openid4java.consumer.ConsumerManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.AuthenticationUserDetailsService;
import org.springframework.security.openid.*;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import java.util.Collections;

@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {
    @Autowired
    private AuthenticationUserDetailsService<OpenIDAuthenticationToken> userDetailsService;

    @Autowired
    private AuthenticationSuccessHandler authenticationSuccessHandler;
    @Autowired
    private AuthenticationFailureHandler authenticationFailureHandler;

    /*
    @Value("${server.host}")
    private String host;// = "http://gamestart.loc";

    public Filter originHolderFilter() {
        return new GenericFilterBean() {
            @Override
            public void doFilter(
                    ServletRequest request,
                    ServletResponse response,
                    FilterChain chain
            ) throws IOException, ServletException {
                final HttpServletRequestWrapper wrapped = new HttpServletRequestWrapper((HttpServletRequest) request) {
                    @Override
                    public StringBuffer getRequestURL() {
                        return new StringBuffer(host).append(super.getRequestURI());
                    }

                    @Override
                    public String getRemoteHost() {
                        return host;
                    }
                };
                chain.doFilter(wrapped, response);
            }
        };
    }
    */

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .addFilterBefore(openIDAuthenticationFilter(), BasicAuthenticationFilter.class)
                //.addFilterBefore(originHolderFilter(), OpenIDAuthenticationFilter.class)
                .csrf().disable()
                .authorizeRequests()
                .anyRequest().permitAll()
                .and().logout().logoutSuccessUrl("/");
        //.and()
        //.openidLogin()
        //.authenticationUserDetailsService(userDetailsService);
                /*

                .anyRequest().authenticated()
                .and()
                .exceptionHandling()
                .authenticationEntryPoint(new Http401AuthenticationEntryPoint(""))
                */
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .authenticationProvider(openIDAuthenticationProvider());
    }


    public OpenIDAuthenticationProvider openIDAuthenticationProvider() {
        OpenIDAuthenticationProvider provider = new OpenIDAuthenticationProvider();
        provider.setAuthenticationUserDetailsService(userDetailsService);
        return provider;
    }

    public OpenIDAuthenticationFilter openIDAuthenticationFilter() throws Exception {
        OpenIDAuthenticationFilter filter = new OpenIDAuthenticationFilter();
        filter.setConsumer(openIDConsumer());
        filter.setAuthenticationManager(authenticationManager());
        filter.setReturnToUrlParameters(Collections.singleton("destination_url"));
        filter.setAuthenticationSuccessHandler(authenticationSuccessHandler);
        filter.setAuthenticationFailureHandler(authenticationFailureHandler);
        return filter;
    }

    @Bean
    public OpenIDConsumer openIDConsumer() throws ConsumerException {
        ConsumerManager consumerManager = new ConsumerManager();
        consumerManager.setMaxAssocAttempts(0);
        return new OpenID4JavaConsumer(
                consumerManager,
                new NullAxFetchListFactory()
        );
    }

    @Override
    protected AuthenticationManager authenticationManager() throws Exception {
        return super.authenticationManager();
    }
}
