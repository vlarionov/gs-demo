package com.gamestart.server.controller.websocket;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.LongNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.gamestart.server.model.bots.Bot;
import com.gamestart.server.model.item.SteamItemIdentifier;
import com.gamestart.server.model.item.TradeOperation;
import com.gamestart.server.repository.BotRepository;
import com.gamestart.server.service.TradeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.core.MessageSendingOperations;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Controller
public class BotController {
    private static final Logger LOG = LoggerFactory.getLogger(BotController.class);

    private static final String MANAGER_READY = "/manager-ready";
    private static final String SEND_BOT_LIST_QUEUE = "/bots";

    private static final String OFFER_SEND_QUEUE = "/offer/send";
    private static final String OFFER_SEND_RESULT_QUEUE = "/offer/send/result";
    private static final String OFFER_SEND_STATUS_CHANGED_QUEUE = "/offer/send/status-changed";

    private final Random random = new Random();

    private final ObjectMapper objectMapper = new ObjectMapper();

    private final MessageSendingOperations<String> messageOperations;

    private final BotRepository botRepository;

    private final TradeService tradeService;

    @Autowired
    public BotController(
            MessageSendingOperations<String> messageOperations,
            BotRepository botRepository,
            TradeService tradeService
    ) {
        this.messageOperations = messageOperations;
        this.botRepository = botRepository;
        this.tradeService = tradeService;
    }


    @MessageMapping(MANAGER_READY)
    public void onManagerReady() {
        messageOperations.convertAndSend(
                SEND_BOT_LIST_QUEUE,
                findAllBotProfiles()
        );
    }

    @Transactional(readOnly = true)
    private List<BotDto> findAllBotProfiles() {
        return botRepository.findAll()
                .stream()
                .map(bot -> new BotDto(
                        bot.getLogin(),
                        bot.getPassword(),
                        bot.getSharedSecret(),
                        bot.getIdentitySecret(),
                        bot.getTradeUrl()
                )).collect(Collectors.toList());
    }

    public void sendOffer(
            Long tradeOperationId,
            String steamId,
            String tradeToken,
            List<SteamItemIdentifier> itemsToGive,
            List<SteamItemIdentifier> itemsToReceive
    ) {
        List<Bot> bots = botRepository.findAll();
        String botId = bots.get(random.nextInt(bots.size())).getLogin();
        ObjectNode request = objectMapper.createObjectNode();
        request.set("id", new LongNode(tradeOperationId));
        request.set("idBot", new TextNode(botId));
        request.set("steamId", new TextNode(steamId));
        request.set("token", new TextNode(tradeToken));
        request.set("itemsToGive", createItemsNodes(itemsToGive));
        request.set("itemsToReceive", createItemsNodes(itemsToReceive));
        messageOperations.convertAndSend(OFFER_SEND_QUEUE, request);
    }

    private ArrayNode createItemsNodes(List<SteamItemIdentifier> items) {
        ArrayNode itemNodes = objectMapper.createArrayNode();
        items.forEach(item -> {
            ObjectNode itemNode = objectMapper.createObjectNode();
            itemNode.set("appid", new TextNode(item.getSteamGame().getAppId()));
            itemNode.set("assetid", new TextNode(item.getAssetId()));
            itemNode.set("contextid", new TextNode(item.getContextId()));
            itemNodes.add(itemNode);
        });
        return itemNodes;
    }

    @MessageMapping(OFFER_SEND_RESULT_QUEUE)
    public void onOfferSendResult(ObjectNode offerMessage) {
        Long operationId = offerMessage.get("id").asLong();

        JsonNode error = offerMessage.get("error");
        if (error != null) {
            LOG.error(String.format(
                    "Trade offer with id=%s error: %s",
                    operationId.toString(),
                    error.toString()
            ));
            tradeService.changeOperationStatusById(operationId, TradeOperation.Status.ERROR);
            return;
        }

        JsonNode offer = offerMessage.get("offer");

        String offerId = offer.get("id").asText();
        tradeService.setOperationOfferId(operationId, offerId);

        String state = offer.get("state").asText();

        if ("CreatedNeedsConfirmation".equals(state)) {
            return;
        }

        if (!"Active".equals(state)) {
            LOG.error(String.format(
                    "Trade offer with id=%s has state '%s', but expected 'Active'",
                    operationId.toString(),
                    state
            ));
            tradeService.changeOperationStatusById(operationId, TradeOperation.Status.ERROR);
            return;
        }

        tradeService.changeOperationStatusById(operationId, TradeOperation.Status.ACTIVE);
    }

    @MessageMapping(OFFER_SEND_STATUS_CHANGED_QUEUE)
    public void onOfferSendStatusChanged(ObjectNode statusChangedMessage) {
        JsonNode offer = statusChangedMessage.get("offer");
        String offerId = offer.get("id").asText();
        String state = offer.get("state").asText();

        TradeOperation.Status status = null;
        if ("Active".equals(state)) {
            status = TradeOperation.Status.ACTIVE;
        } else if ("Declined".equals(state)) {
            status = TradeOperation.Status.DECLINED;
        } else if ("Accepted".equals(state)) {
            status = TradeOperation.Status.COMPLETE;
        }
        if (status != null) {
            tradeService.changeOperationStatusByOfferId(offerId, status);
        } else {
            LOG.error(String.format(
                    "Trade offer with offerId=%s has unknown state '%s'",
                    offerId,
                    state
            ));
        }

    }

    public static class BotDto {
        public String login;
        public String password;
        public String sharedSecret;
        public String identitySecret;
        public String tradeUrl;

        public BotDto(String login, String password, String sharedSecret, String identitySecret, String tradeUrl) {
            this.login = login;
            this.password = password;
            this.sharedSecret = sharedSecret;
            this.identitySecret = identitySecret;
            this.tradeUrl = tradeUrl;
        }
    }
}