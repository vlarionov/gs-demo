package com.gamestart.server.controller.http.dto;

import java.util.List;

public class PageDto<T> {
    public int number;
    public int size;
    public int total;

    public List<T> body;

    public PageDto() {
    }

    public PageDto(int number, int size, int total, List<T> body) {
        this.number = number;
        this.size = size;
        this.total = total;
        this.body = body;
    }
}
