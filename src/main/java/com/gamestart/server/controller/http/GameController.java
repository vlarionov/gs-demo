package com.gamestart.server.controller.http;

import com.gamestart.server.controller.http.dto.PageDto;
import com.gamestart.server.controller.http.dto.UserLinkedDto;
import com.gamestart.server.exception.InvalidRequestException;
import com.gamestart.server.model.game.Bet;
import com.gamestart.server.model.game.Game;
import com.gamestart.server.model.game.UserStats;
import com.gamestart.server.model.user.User;
import com.gamestart.server.service.GameService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/game")
public class GameController {
    private final GameService gameService;

    @Autowired
    public GameController(GameService gameService) {
        this.gameService = gameService;
    }

    @RequestMapping(value = "/current", method = RequestMethod.GET)
    public CurrentGameDto getCurrent() {
        Game game = gameService.getCurrent();
        if (game.getStatus() == Game.GameStatus.WAITING) {
            return new WaitingGameDto(game);
        }
        Long timeToNextStatus = gameService.getTimeToNewStatus(game);
        if (timeToNextStatus < 0) {
            gameService.refreshStatus();
            return getCurrent();
        }
        switch (game.getStatus()) {
            case ACTIVE:
                return new ActiveGameDto(game, timeToNextStatus);
            case DRAWING:
                return new DrawingGameDto(game, timeToNextStatus);
            case CLOSING:
                return new ClosingGameDto(game, timeToNextStatus);
            default:
                return null;
        }
    }

    @RequestMapping(value = "/currentBets", method = RequestMethod.GET)
    public List<BetDto> getBets() {
        Game game = gameService.getCurrent();
        if (game == null) {
            return null;
        }
        return game.getBets().stream()
                .map(BetDto::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/bets", method = RequestMethod.GET)
    public List<BetDto> getBets(
            @RequestParam Long gameId
    ) {
        List<Bet> bets = gameService.getBets(gameId);
        if (bets == null) {
            return null;
        }
        return bets.stream()
                .map(BetDto::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(method = RequestMethod.GET)
    public ClosedGameDto getGame(
            @RequestParam Long gameId
    ) {
        Game game = gameService.findFinishedGame(gameId);
        if (game == null) {
            return null;
        }
        return new ClosedGameDto(game);
    }

    @RequestMapping(value = "/history", method = RequestMethod.GET)
    public PageDto<ClosedGameDto> getGames(
            @RequestParam int pageNumber,
            @RequestParam int pageSize
    ) {
        if (pageNumber < 0 || pageSize <= 0) {
            throw new InvalidRequestException();
        }
        Page<Game> gamePage = gameService.getHistory(pageNumber, pageSize);
        List<ClosedGameDto> closedGameDtoList = gamePage.getContent()
                .stream().map(ClosedGameDto::new).collect(Collectors.toList());
        return new PageDto<>(
                pageNumber,
                pageSize,
                gamePage.getTotalPages(),
                closedGameDtoList
        );
    }

    @RequestMapping(value = "/userStats", method = RequestMethod.GET)
    public UserStatsDto getUserStats() {
        return new UserStatsDto(
                gameService.getUserStats(AuthUtils.getUserId())
        );
    }

    @RequestMapping(value = "/topUsersStats", method = RequestMethod.GET)
    public List<UserStatsDto> getTopUsersStats() {
        return gameService.geTopUsersStats().stream()
                .map(UserStatsDto::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/bet", method = RequestMethod.POST)
    public void createBet(
            @RequestBody @Valid BetRequest bet
    ) {
        gameService.createBet(
                bet.gameId,
                AuthUtils.getUserId(),
                bet.amount,
                bet.type
        );
    }

    @RequestMapping(value = "/betHistory", method = RequestMethod.GET)
    public PageDto<ClosedBetDto> getBetHistory(
            @RequestParam int pageNumber,
            @RequestParam int pageSize
    ) {
        if (pageNumber < 0 || pageSize <= 0) {
            throw new InvalidRequestException();
        }
        Page<Bet> betPage = gameService.getUserBets(
                AuthUtils.getUserId(),
                pageNumber,
                pageSize
        );
        List<ClosedBetDto> betDtoList = betPage.getContent().stream()
                .map(bet -> new ClosedBetDto(
                        DateUtils.toISO(bet.getDate()),
                        bet.getType().name(),
                        bet.getAmount(),
                        bet.isWin(),
                        bet.getProfit()
                )).collect(Collectors.toList());
        return new PageDto<>(
                pageNumber,
                pageSize,
                betPage.getTotalPages(),
                betDtoList
        );
    }


    public static class BetRequest {
        @NotNull
        public Long gameId;

        @Min(1)
        public int amount;

        @NotNull
        public Bet.Type type;

        public BetRequest() {
        }

    }

    public static class ClosedGameDto {
        public Long id;

        public int number;

        public long bettingTime;
        public long drawingTime;
        public long closingTime;

        public String startDate;
        public String finishDate;

        public int minBet;
        public int maxBet;
        public int maxBetCount;
        public List<BetDto> bets;

        public double roundNumber;

        public String roundHash;

        public Integer winNumber;

        public ClosedGameDto(Long id, int number, long bettingTime, long drawingTime, long closingTime, String startDate, String finishDate, int minBet, int maxBet, int maxBetCount, List<BetDto> bets, double roundNumber, String roundHash, Integer winNumber) {
            this.id = id;
            this.number = number;
            this.bettingTime = bettingTime;
            this.drawingTime = drawingTime;
            this.closingTime = closingTime;
            this.startDate = startDate;
            this.finishDate = finishDate;
            this.minBet = minBet;
            this.maxBet = maxBet;
            this.maxBetCount = maxBetCount;
            this.bets = bets;
            this.roundNumber = roundNumber;
            this.roundHash = roundHash;
            this.winNumber = winNumber;
        }

        public ClosedGameDto(Game game) {
            this(
                    game.getId(),
                    game.getNumber(),
                    game.getBettingTime(),
                    game.getDrawingTime(),
                    game.getClosingTime(),
                    DateUtils.toISO(game.getStartDate()),
                    DateUtils.toISO(game.getFinishDate()),
                    game.getMinBet(),
                    game.getMaxBet(),
                    game.getMaxBetCount(),
                    game.getBets().stream().map(BetDto::new).collect(Collectors.toList()),
                    game.getRoundNumber(),
                    game.getRoundHash(),
                    game.getWinNumber()
            );
        }
    }

    public static abstract class CurrentGameDto {
        public Long id;
        public int number;
        public int minBet;
        public int maxBet;
        public int maxBetCount;

        public String roundHash;

        public long bettingTime;
        public long drawingTime;
        public long closingTime;

        public String status;
        public Long remainingTime;

        public CurrentGameDto(Long id, int number, int minBet, int maxBet, int maxBetCount, String roundHash, long bettingTime, long drawingTime, long closingTime, String status, Long remainingTime) {
            this.id = id;
            this.number = number;
            this.minBet = minBet;
            this.maxBet = maxBet;
            this.maxBetCount = maxBetCount;
            this.roundHash = roundHash;
            this.bettingTime = bettingTime;
            this.drawingTime = drawingTime;
            this.closingTime = closingTime;
            this.status = status;
            this.remainingTime = remainingTime;
        }
    }

    public static class WaitingGameDto extends CurrentGameDto {
        public WaitingGameDto(Long id, int number, int minBet, int maxBet, int maxBetCount, String roundHash, long bettingTime, long drawingTime, long closingTime) {
            super(id, number, minBet, maxBet, maxBetCount, roundHash, bettingTime, drawingTime, closingTime, Game.GameStatus.WAITING.name(), null);
        }

        public WaitingGameDto(Game game) {
            this(
                    game.getId(),
                    game.getNumber(),
                    game.getMinBet(),
                    game.getMaxBet(),
                    game.getMaxBetCount(),
                    game.getRoundHash(),
                    game.getBettingTime(),
                    game.getDrawingTime(),
                    game.getClosingTime()
            );
        }


    }

    public static class ActiveGameDto extends CurrentGameDto {
        public ActiveGameDto(Long id, int number, int minBet, int maxBet, int maxBetCount, String roundHash, long bettingTime, long drawingTime, long closingTime, Long timeToNewStatus) {
            super(id, number, minBet, maxBet, maxBetCount, roundHash, bettingTime, drawingTime, closingTime, Game.GameStatus.ACTIVE.name(), timeToNewStatus);
        }

        public ActiveGameDto(Game game, long timeToNextStatus) {
            this(
                    game.getId(),
                    game.getNumber(),
                    game.getMinBet(),
                    game.getMaxBet(),
                    game.getMaxBetCount(),
                    game.getRoundHash(),
                    game.getBettingTime(),
                    game.getDrawingTime(),
                    game.getClosingTime(),
                    timeToNextStatus
            );
        }

    }

    public static class DrawingGameDto extends CurrentGameDto {
        public double roundNumber;

        public int winNumber;

        public DrawingGameDto(Long id, int number, int minBet, int maxBet, int maxBetCount, String roundHash, long bettingTime, long drawingTime, long closingTime, Long timeToNewStatus, double roundNumber, int winNumber) {
            super(id, number, minBet, maxBet, maxBetCount, roundHash, bettingTime, drawingTime, closingTime, Game.GameStatus.DRAWING.name(), timeToNewStatus);
            this.roundNumber = roundNumber;
            this.winNumber = winNumber;
        }

        public DrawingGameDto(Game game, long timeToNextStatus) {
            this(
                    game.getId(),
                    game.getNumber(),
                    game.getMinBet(),
                    game.getMaxBet(),
                    game.getMaxBetCount(),
                    game.getRoundHash(),
                    game.getBettingTime(),
                    game.getDrawingTime(),
                    game.getClosingTime(),
                    timeToNextStatus,
                    game.getRoundNumber(),
                    game.getWinNumber()
            );
        }
    }

    public static class ClosingGameDto extends CurrentGameDto {
        public double roundNumber;

        public int winNumber;

        public ClosingGameDto(Long id, int number, int minBet, int maxBet, int maxBetCount, String roundHash, long bettingTime, long drawingTime, long closingTime, Long timeToNewStatus, double roundNumber, int winNumber) {
            super(id, number, minBet, maxBet, maxBetCount, roundHash, bettingTime, drawingTime, closingTime, Game.GameStatus.CLOSING.name(), timeToNewStatus);
            this.roundNumber = roundNumber;
            this.winNumber = winNumber;
        }

        public ClosingGameDto(Game game, long timeToNextStatus) {
            this(
                    game.getId(),
                    game.getNumber(),
                    game.getMinBet(),
                    game.getMaxBet(),
                    game.getMaxBetCount(),
                    game.getRoundHash(),
                    game.getBettingTime(),
                    game.getDrawingTime(),
                    game.getClosingTime(),
                    timeToNextStatus,
                    game.getRoundNumber(),
                    game.getWinNumber()
            );
        }
    }

    public static class BetDto extends UserLinkedDto {
        public Long id;

        public String type;
        public double amount;

        public BetDto(User user, Long id, String type, double amount) {
            super(user);
            this.id = id;
            this.type = type;
            this.amount = amount;
        }

        public BetDto(Bet bet) {
            this(
                    bet.getUser(),
                    bet.getId(),
                    bet.getType().name(),
                    bet.getAmount()
            );
        }
    }

    public static class ClosedBetDto {
        public String date;
        public String type;
        public int amount;
        public boolean win;
        public int profit;

        public ClosedBetDto(String date, String type, int amount, boolean win, int profit) {
            this.date = date;
            this.type = type;
            this.amount = amount;
            this.win = win;
            this.profit = profit;
        }
    }

    public static class UserStatsDto {
        public int betsCount;
        public int totalProfit;

        public UserStatsDto(int betsCount, int totalProfit) {
            this.betsCount = betsCount;
            this.totalProfit = totalProfit;
        }

        public UserStatsDto(UserStats stats) {
            this(
                    stats.getBetsCount(),
                    stats.getTotalProfit()
            );
        }
    }

    public static class TopUserStatsDto extends UserLinkedDto {
        public int betsCount;
        public int totalProfit;

        public TopUserStatsDto(User user, int betsCount, int totalProfit) {
            super(user);
            this.betsCount = betsCount;
            this.totalProfit = totalProfit;
        }

        public TopUserStatsDto(UserStats stats) {
            this(
                    stats.getUser(),
                    stats.getBetsCount(),
                    stats.getTotalProfit()
            );
        }
    }

}
