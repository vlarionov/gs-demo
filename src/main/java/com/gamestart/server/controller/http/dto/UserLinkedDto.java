package com.gamestart.server.controller.http.dto;

import com.gamestart.server.model.user.User;

public class UserLinkedDto {
    public Long userId;
    public String userName;

    public String userAvatarMinUrl;
    public String userAvatarMediumUrl;
    public String userAvatarMaxUrl;

    public String userProfileUrl;

    public UserLinkedDto(Long userId, String userName, String userAvatarMinUrl, String userAvatarMediumUrl, String userAvatarMaxUrl, String userProfileUrl) {
        this.userId = userId;
        this.userName = userName;
        this.userAvatarMinUrl = userAvatarMinUrl;
        this.userAvatarMediumUrl = userAvatarMediumUrl;
        this.userAvatarMaxUrl = userAvatarMaxUrl;
        this.userProfileUrl = userProfileUrl;
    }

    public UserLinkedDto(User user) {
        this(
                user.getId(),
                user.getName(),
                user.getSteamInfo().getAvatarMinUrl(),
                user.getSteamInfo().getAvatarMediumUrl(),
                user.getSteamInfo().getAvatarMaxUrl(),
                user.getSteamInfo().getProfileUrl()
        );
    }
}
