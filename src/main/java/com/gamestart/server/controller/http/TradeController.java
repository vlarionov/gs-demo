package com.gamestart.server.controller.http;

import com.gamestart.server.controller.http.dto.PageDto;
import com.gamestart.server.exception.InvalidRequestException;
import com.gamestart.server.exception.TradeOfferUrlNotProvidedException;
import com.gamestart.server.model.SteamGame;
import com.gamestart.server.model.item.SteamItem;
import com.gamestart.server.model.item.SteamItemIdentifier;
import com.gamestart.server.model.item.TradeOperation;
import com.gamestart.server.model.user.TradeToken;
import com.gamestart.server.repository.UserRepository;
import com.gamestart.server.service.SteamService;
import com.gamestart.server.service.TradeService;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequestMapping("/trade")
public class TradeController {
    private final UserRepository userRepository;

    private final SteamService steamService;
    private final TradeService tradeService;

    @Autowired
    public TradeController(
            SteamService steamService,
            TradeService tradeService,
            UserRepository userRepository
    ) {
        this.steamService = steamService;
        this.tradeService = tradeService;
        this.userRepository = userRepository;
    }

    //region sellItems
    @RequestMapping(value = "/inventory", method = RequestMethod.GET)
    public List<SteamItemDto> getInventory(
            @RequestParam SteamGame steamGame
    ) {
        List<SteamItem> steamItems = steamService.getInventory(
                userRepository.findOne(AuthUtils.getUserId()).getSteamInfo().getSteamId(),
                steamGame
        );
        /*
        Collections.sort(
                items,
                (i1, i2) -> i2.price * i2.amount - i1.price * i1.amount > 0 ? 1 : -1
        );
        */
        return steamItems.stream()
                .map(SteamItemDto::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/sellItems", method = RequestMethod.POST)
    public TradeOperationDto sellItems(
            @RequestBody @Valid SellItemsRequest request
    ) {
        checkTradeOfferUrl();
        List<SteamItemIdentifier> itemsIdentifiers = request.items.stream()
                .map(itemIdentifierDto -> new SteamItemIdentifier(
                        itemIdentifierDto.steamGame,
                        itemIdentifierDto.contextId,
                        itemIdentifierDto.assetId
                )).collect(Collectors.toList());
        TradeOperation tradeOperation = tradeService.sellItems(
                userRepository.findOne(AuthUtils.getUserId()),
                itemsIdentifiers
        );
        return new TradeOperationDto(tradeOperation);
    }

    public static class SellItemsRequest {
        @NotEmpty
        @Valid
        public List<ItemIdentifierDto> items;

        public SellItemsRequest() {
        }

    }
    //endregion

    //region buyItems
    @RequestMapping(value = "/itemTagsCategories", method = RequestMethod.GET)
    public Set<String> getItemTagCategories(
            @RequestParam SteamGame steamGame
    ) {
        return tradeService.getCategories(steamGame);
    }

    @RequestMapping(value = "/itemTags", method = RequestMethod.GET)
    public Set<String> getItemTags(
            @RequestParam SteamGame steamGame,
            @RequestParam @NotEmpty String category
    ) {
        return tradeService.getTags(steamGame, category);
    }

    @RequestMapping(value = "/storeItems", method = RequestMethod.POST)
    public PageDto<SteamItemDto> getStoreItems(
            @RequestBody @Valid GetStoreItemsRequest request
    ) {
        Stream<SteamItem> itemsStream = tradeService.getStoreItems(request.steamGame).stream();
        if (request.searchString != null) {
            itemsStream = itemsStream.filter(
                    item -> item.getName().toLowerCase().contains(request.searchString.toLowerCase())
            );
        }
        if (request.tags != null) {
            for (GetStoreItemsRequest.Tag tag : request.tags) {
                itemsStream = itemsStream.filter(
                        item -> tag.name.equals(item.getTags().get(tag.category))
                );
            }
        }
        List<SteamItem> items = itemsStream.collect(Collectors.toList());

        int totalPages = (items.size() / request.pageSize)
                + (items.size() % request.pageSize > 0 ? 1 : 0);

        return new PageDto<>(
                request.pageNumber,
                request.pageSize,
                totalPages,
                items.stream()
                        .skip(request.pageNumber * request.pageSize)
                        .limit(request.pageSize)
                        .map(SteamItemDto::new)
                        .collect(Collectors.toList())
        );
    }

    @RequestMapping(value = "/buyItem", method = RequestMethod.POST)
    public TradeOperationDto buyItem(
            @RequestBody @Valid ItemIdentifierDto request
    ) {
        checkTradeOfferUrl();
        TradeOperation tradeOperation = tradeService.buyItem(
                userRepository.findOne(AuthUtils.getUserId()),
                new SteamItemIdentifier(
                        request.steamGame,
                        request.contextId,
                        request.assetId
                )
        );
        return new TradeOperationDto(tradeOperation);
    }

    public static class GetStoreItemsRequest {
        @Min(0)
        public int pageNumber;
        @Min(1)
        public int pageSize;

        @NotNull
        public SteamGame steamGame;

        public String searchString;

        @Valid
        public List<Tag> tags;

        public static class Tag {
            @NotBlank
            public String category;
            @NotBlank
            public String name;
        }
    }
    //endregion

    @RequestMapping(value = "/tradeHistory", method = RequestMethod.GET)
    public PageDto<TradeOperationDto> getTradeOperationsHistory(
            @RequestParam int pageNumber,
            @RequestParam int pageSize,
            @RequestParam TradeOperation.Type type
    ) {
        if (pageNumber < 0 || pageSize <= 0) {
            throw new InvalidRequestException();
        }
        Page<TradeOperation> page = tradeService.getTradeOperationsHistory(
                AuthUtils.getUserId(),
                type,
                pageNumber,
                pageSize
        );
        List<TradeOperationDto> tradeOperations = page.getContent().stream()
                .map(TradeOperationDto::new)
                .collect(Collectors.toList());
        return new PageDto<>(
                pageNumber,
                pageSize,
                page.getTotalPages(),
                tradeOperations
        );
    }

    private void checkTradeOfferUrl() {
        TradeToken tradeToken = userRepository.findOne(AuthUtils.getUserId()).getTradeToken();
        if (tradeToken == null) {
            throw new TradeOfferUrlNotProvidedException();
        }
    }

    public static class SteamItemDto {
        public String steamGame;
        public String contextId;
        public String assetId;

        public String name;
        public String imageUrl;

        public int amount;
        public double price;

        public List<Tag> tags;

        public SteamItemDto() {
        }

        public SteamItemDto(String steamGame, String contextId, String assetId, String name, String imageUrl, int amount, double price, List<Tag> tags) {
            this.steamGame = steamGame;
            this.contextId = contextId;
            this.assetId = assetId;
            this.name = name;
            this.imageUrl = imageUrl;
            this.amount = amount;
            this.price = price;
            this.tags = tags;
        }

        public SteamItemDto(SteamItem item) {
            this(
                    item.getIdentifier().getSteamGame().name(),
                    item.getIdentifier().getContextId(),
                    item.getIdentifier().getAssetId(),
                    item.getName(),
                    item.getIconUrl(),
                    item.getAmount(),
                    item.getPrice(),
                    item.getTags().keySet().stream()
                            .map(category -> new Tag(category, item.getTags().get(category)))
                            .collect(Collectors.toList())
            );
        }

        public static class Tag {
            public String category;

            public String name;

            public Tag(String category, String name) {
                this.category = category;
                this.name = name;
            }
        }
    }

    public static class ItemIdentifierDto {
        @NotNull
        public SteamGame steamGame;
        @NotEmpty
        public String contextId;
        @NotEmpty
        public String assetId;

        public ItemIdentifierDto() {
        }

    }

    public static class TradeOperationDto {
        public Long id;

        public String type;

        public String date;

        public String status;

        public List<SteamItemDto> items;

        public TradeOperationDto() {
        }

        public TradeOperationDto(Long id, String type, String date, String status, List<SteamItemDto> items) {
            this.id = id;
            this.type = type;
            this.date = date;
            this.status = status;
            this.items = items;
        }

        public TradeOperationDto(TradeOperation operation) {
            this(
                    operation.getId(),
                    operation.getType().name(),
                    DateUtils.toISO(operation.getDate()),
                    operation.getStatus().name(),
                    operation.getItems().stream()
                            .map(SteamItemDto::new)
                            .collect(Collectors.toList())
            );
        }
    }

}
