package com.gamestart.server.controller.http;

import com.gamestart.server.exception.NotAuthorizedException;
import com.gamestart.server.model.user.User;
import com.gamestart.server.model.user.UserAuthDetails;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

public class AuthUtils {

    public static Long getUserId() {
        return getUser().getId();
    }

    private static User getUser() throws NotAuthorizedException {
        Authentication authentication = getAuthentication();
        if (authentication == null) {
            throw new NotAuthorizedException();
        }
        if (authentication.getPrincipal() instanceof UserAuthDetails) {
            return ((UserAuthDetails) authentication.getPrincipal()).getUser();
        } else {
            throw new NotAuthorizedException();
        }
    }

    private static Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

}