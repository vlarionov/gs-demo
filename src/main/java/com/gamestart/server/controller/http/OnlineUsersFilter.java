package com.gamestart.server.controller.http;

import com.gamestart.server.exception.NotAuthorizedException;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.time.Instant;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class OnlineUsersFilter extends OncePerRequestFilter {
    private static final long MAX_WAITING_TIME = 15 * 60 * 1000L;
    private static final long CHECK_EXPIRED_DATES_TIME = 10 * 1000L;
    private final Map<Long, Instant> lastUserRequestDates = new ConcurrentHashMap<>();

    public int getOnlineUsersCount() {
        return lastUserRequestDates.size();
    }

    @Scheduled(fixedRate = CHECK_EXPIRED_DATES_TIME)
    private void checkExpiredDates() {
        Instant now = Instant.now();
        for (Long userId : lastUserRequestDates.keySet()) {
            boolean expired = lastUserRequestDates.get(userId)
                    .plusMillis(MAX_WAITING_TIME)
                    .isBefore(now);
            if (expired) {
                lastUserRequestDates.remove(userId);
            }
        }
    }


    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            lastUserRequestDates.put(
                    AuthUtils.getUserId(),
                    Instant.now()
            );
        } catch (NotAuthorizedException ignored) {
        } finally {
            filterChain.doFilter(request, response);
        }
    }
}
