package com.gamestart.server.controller.http;

import com.gamestart.server.exception.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;


@ControllerAdvice
public class ExceptionHandlingController {
    private static final Logger lOG = LoggerFactory.getLogger(ExceptionHandlingController.class);

    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(Throwable.class)
    public void serverException(Throwable throwable) {
        lOG.error("Server error:", throwable);
    }

    @ResponseStatus(value = HttpStatus.FORBIDDEN, reason = "Not authorized")
    @ExceptionHandler(NotAuthorizedException.class)
    public void notAuthorizedException() {
    }

    //todo not worked
    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid request arguments")
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public void methodArgumentNotValidException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid request")
    @ExceptionHandler(InvalidRequestException.class)
    public void invalidRequestException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Data integrity violation")
    @ExceptionHandler(DataIntegrityViolationException.class)
    public void dataIntegrityViolationException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Steam user does not found")
    @ExceptionHandler(SteamUserNotFoundException.class)
    public void steamUserNotFoundException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Trade offer url does not provided")
    @ExceptionHandler(TradeOfferUrlNotProvidedException.class)
    public void tradeOfferUrlNotProvidedException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Game closed")
    @ExceptionHandler(GameClosedException.class)
    public void gameClosedException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Item not found")
    @ExceptionHandler(ItemNotFoundException.class)
    public void itemNotFoundException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Not enough balance")
    @ExceptionHandler(NotEnoughBalanceException.class)
    public void notEnoughBalanceException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid market info")
    @ExceptionHandler(InvalidItemMarketInfoException.class)
    public void invalidItemMarketInfoException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Item tag not found")
    @ExceptionHandler(ItemTagNotFoundException.class)
    public void itemTagNotFoundException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Invalid bet")
    @ExceptionHandler(InvalidBetException.class)
    public void invalidBetException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Reached the maximum number of bets")
    @ExceptionHandler(MaxBetCountException.class)
    public void maxBetCountException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Steam game does not supported")
    @ExceptionHandler(SteamGameNotSupportedException.class)
    public void steamGameNotSupportedException() {
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST, reason = "Inventory does not available")
    @ExceptionHandler(InventoryNotAvailableException.class)
    public void inventoryNotAvailableException() {
    }


}
