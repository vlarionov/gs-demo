package com.gamestart.server.controller.http;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class DateUtils {
    private static final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");

    static {
        dateFormat.setTimeZone(TimeZone.getDefault());
    }

    public static String toISO(Date date) {
        return date != null ? dateFormat.format(date) : null;
    }
}
