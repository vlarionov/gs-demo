package com.gamestart.server.controller.http;

import com.gamestart.server.controller.http.dto.UserLinkedDto;
import com.gamestart.server.exception.InvalidRequestException;
import com.gamestart.server.model.chat.ChatMessage;
import com.gamestart.server.model.user.User;
import com.gamestart.server.service.ChatService;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Size;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/chat")
public class ChatController {
    private static final int MAX_MESSAGE_SIZE = 256;

    private final ChatService chatService;

    @Autowired
    public ChatController(ChatService chatService) {
        this.chatService = chatService;
    }

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    public List<MessageDto> getMessages(
            @RequestParam int pageNumber,
            @RequestParam int pageSize
    ) {
        if (pageNumber < 0 || pageSize <= 0) {
            throw new InvalidRequestException();
        }
        List<ChatMessage> messages = chatService.getMessages(
                pageNumber,
                pageSize
        );
        return messages.stream()
                .map(MessageDto::new)
                .collect(Collectors.toList());
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public void sendMessage(@RequestBody @Valid MessageRequest request) {
        chatService.sendMessage(AuthUtils.getUserId(), request.message);
    }

    public static class MessageRequest {
        @NotBlank
        @Size(max = MAX_MESSAGE_SIZE)
        public String message;

    }

    public static class MessageDto extends UserLinkedDto {
        public Long id;
        public String date;
        public String text;

        public MessageDto(User user, Long id, String date, String text) {
            super(user);
            this.id = id;
            this.date = date;
            this.text = text;
        }

        public MessageDto(ChatMessage message) {
            this(
                    message.getUser(),
                    message.getId(),
                    DateUtils.toISO(message.getDate()),
                    message.getText()
            );
        }
    }
}
