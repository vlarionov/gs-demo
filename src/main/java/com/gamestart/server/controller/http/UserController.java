package com.gamestart.server.controller.http;

import com.gamestart.server.model.user.User;
import com.gamestart.server.repository.UserRepository;
import com.gamestart.server.service.UserService;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.URL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/user")
public class UserController {
    private final UserRepository userRepository;

    private final UserService userService;

    private final OnlineUsersFilter onlineUsersFilter;

    @Autowired
    public UserController(
            UserRepository userRepository,
            UserService userService,
            OnlineUsersFilter onlineUsersFilter
    ) {
        this.userRepository = userRepository;
        this.userService = userService;
        this.onlineUsersFilter = onlineUsersFilter;
    }

    @RequestMapping(value = "/onlineUsersCount", method = RequestMethod.GET)
    public int onlineUsersCount() {
        return onlineUsersFilter.getOnlineUsersCount();
    }

    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public UserDto getProfile() {
        User user = userRepository.findOne(AuthUtils.getUserId());
        return new UserDto(user);
    }

    @RequestMapping(value = "/provideTradeOfferUrl", method = RequestMethod.POST)
    public void provideTradeOfferUrl(
            @RequestBody @Valid TradeOfferUrlRequest request
    ) {
        userService.saveTradeToken(AuthUtils.getUserId(), request.url);
    }


    public static class UserDto {
        public Long userId;
        public String name;
        public double balance;

        public String tradeOfferUrl;

        public String avatarMinUrl;
        public String avatarMediumUrl;
        public String avatarMaxUrl;

        public UserDto(Long userId, String name, double balance, String tradeOfferUrl, String avatarMinUrl, String avatarMediumUrl, String avatarMaxUrl) {
            this.userId = userId;
            this.name = name;
            this.balance = balance;
            this.tradeOfferUrl = tradeOfferUrl;
            this.avatarMinUrl = avatarMinUrl;
            this.avatarMediumUrl = avatarMediumUrl;
            this.avatarMaxUrl = avatarMaxUrl;
        }

        public UserDto(User user) {
            this(
                    user.getId(),
                    user.getName(),
                    user.getBalance(),
                    user.getTradeToken() != null ? user.getTradeToken().getUrl() : null,
                    user.getSteamInfo().getAvatarMinUrl(),
                    user.getSteamInfo().getAvatarMediumUrl(),
                    user.getSteamInfo().getAvatarMaxUrl()
            );
        }
    }

    public static class TradeOfferUrlRequest {
        @URL
        @NotBlank
        public String url;
    }

}
